import java.awt.EventQueue;

import javax.swing.UIManager;

import nukhy.project.window.Login;
import nukhy.project.window.MainWindow;



public class Main {
	
	public static void main(String[] args) {
		try {
		      UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

	    } catch (Exception e) { System.err.println("Error: " + e.getMessage()); }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					Login login = new Login(window);
					login.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
