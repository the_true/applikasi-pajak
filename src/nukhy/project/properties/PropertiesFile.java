package nukhy.project.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesFile {
	private Properties pros;
	private String server;
	private String port;
	private String database;
	public PropertiesFile(){
		InputStream is=null;
		pros = new Properties();
		try{
			File f = new File("Setting.ini");
			is = new FileInputStream(f);
		}catch(Exception e){ is=null;}
		
		try{
			if(is==null){
				is=getClass().getResourceAsStream("Setting.ini");
			}
			pros.load(is);
		}catch(Exception e){}
		this.server = pros.getProperty("server","localhost");
		this.port   = pros.getProperty("port","1533");
		this.database = pros.getProperty("database");
	}
	
	public Properties getPros() {
		return pros;
	}

	public void setPros(Properties pros) {
		this.pros = pros;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public void saveParamChanges(String database) {
		OutputStream out=null;
	    try {
	        pros.setProperty("server", server);
	        pros.setProperty("port", port);
	        pros.setProperty("database", database);
	        File f = new File("Setting.ini");
	        out = new FileOutputStream( f );
	        pros.store(out, null);
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }finally{
	    	if(out!=null){
	    		try{
	    			out.close();
	    		}catch(IOException e){
	    			e.printStackTrace();
	    		}
	    	}
	    }
	}
}