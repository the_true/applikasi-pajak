package nukhy.project.excel;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import nukhy.project.Data.DataPajakKeluar;
import nukhy.project.ItemData.OFItem;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelExporterPajakKeluaran {
	private Workbook workbook;
	private Sheet fpSheet;
	private ArrayList<DataPajakKeluar> data;
	public static final String[][] title=
		{{"FK","KD_JENIS_TRANSAKSI","FG_PENGGANTI","NOMOR_FAKTUR","MASA_PAJAK","TAHUN_PAJAK","TANGGAL_FAKTUR","NPWP","NAMA","ALAMAT_LENGKAP","JUMLAH_DPP",
			"JUMLAH_PPN","JUMLAH_PPNBM","ID_KETERANGAN_TAMBAHAN","FG_UANG_MUKA","UANG_MUKA_DPP","UANG_MUKA_PPN","UANG_MUKA_PPNBM","REFERENSI"},
		 {"LT","NPWP","NAMA","JALAN","BLOK","NOMOR","RT","RW","KECAMATAN","KELURAHAN","KABUPATEN","PROPINSI","KODE_POS","NOMER_TELEPON"},
		 {"OF","KODE_OBJEK","NAMA","HARGA_SATUAN","JUMLAH_BARANG","HARGA_TOTAL","DISKON","DPP","PPN","TARIF_PPNBM","PPNBM"}
		};
	public ExcelExporterPajakKeluaran(ArrayList<DataPajakKeluar> dt){
		workbook = new XSSFWorkbook();
		fpSheet = workbook.createSheet("Export FP");
		this.data = dt;
		System.out.println("Buat Header...");
		createHeader();
		System.out.println("Buat Content...");
		createContent();
	}
	
	public void createHeader(){
		Row row;
		int rowIndex=0;
		XSSFCellStyle[] style=headerStyle();
		for(int cell=0;cell<title.length;cell++){
			row = fpSheet.createRow(rowIndex++);
			for(int column=0;column<title[0].length;column++){
				if(column>=title[cell].length){
					row.createCell(column).setCellValue("");
					row.getCell(column).setCellStyle(style[cell]);
					continue;
				}
				row.createCell(column).setCellValue(title[cell][column]);
				row.getCell(column).setCellStyle(style[cell]);
			}
		}	
	}
	
	public void createContent(){
		Row row;
		int rowIndex=3;
		XSSFCellStyle[] style = headerStyle();
		int i=0;
		for(DataPajakKeluar sdata:data){
			if(!sdata.exportData) continue;
			System.out.println("Masukin Data "+ i++);
			row = fpSheet.createRow(rowIndex++);
			Object[] fk = sdata.fk.getArrayObject();
			for(int column=1;column<fk.length;column++){
				row.createCell(column-1).setCellValue(fk[column].toString());
				row.getCell(column-1).setCellStyle(style[0]);
				row.getCell(column-1).setCellType(Cell.CELL_TYPE_STRING);
			}
			row = fpSheet.createRow(rowIndex++);
			Object[] lt = sdata.lt.getArrayObject();
			for(int column=1;column<lt.length;column++){
				row.createCell(column-1).setCellValue(lt[column].toString());
				row.getCell(column-1).setCellStyle(style[1]);
				row.getCell(column-1).setCellType(Cell.CELL_TYPE_STRING);
			}
			for(int color=0 ; color<5;color++){
				row.createCell(color+lt.length-1).setCellValue("");
				row.getCell(color+lt.length-1).setCellStyle(style[1]);
				row.getCell(color+lt.length-1).setCellType(Cell.CELL_TYPE_STRING);
			}
			ArrayList<OFItem> ofdatas = sdata.of;
			for(OFItem ofnya: ofdatas){
				row = fpSheet.createRow(rowIndex++);
				Object[] of = ofnya.getArrayObject();
				for(int column=1;column<of.length;column++){
					row.createCell(column-1).setCellValue(of[column].toString());
					row.getCell(column-1).setCellStyle(style[2]);
					row.getCell(column-1).setCellType(Cell.CELL_TYPE_STRING);
				}
				for(int color=0 ; color<8;color++){
					row.createCell(color+of.length-1).setCellValue("");
					row.getCell(color+of.length-1).setCellStyle(style[2]);
					row.getCell(color+of.length-1).setCellType(Cell.CELL_TYPE_STRING);
				}
			}
		}
		autoSizeColumn();
	}
	
	public void autoSizeColumn(){
		for(int i=0;i<19;i++){
			fpSheet.autoSizeColumn(i);
		}
	}
	
	public XSSFCellStyle[] headerStyle(){
		Font font = workbook.createFont();
		//font.setBold(true);
		DataFormat dm = workbook.createDataFormat();
		XSSFCellStyle style1 = (XSSFCellStyle) workbook.createCellStyle();
		style1.setFont(font);
		style1.setFillForegroundColor(new XSSFColor(new Color(0xD8E4BC)));
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style1.setDataFormat(dm.getFormat("@"));
		XSSFCellStyle style2 = (XSSFCellStyle) workbook.createCellStyle();
		style2.setFont(font);
		style2.setFillForegroundColor(new XSSFColor(new Color(0xFCD5B4)));
		style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style2.setDataFormat(dm.getFormat("@"));
		XSSFCellStyle style3 = (XSSFCellStyle) workbook.createCellStyle();
		style3.setFont(font);
		style3.setFillForegroundColor(new XSSFColor(new Color(0xFFFF00)));
		style3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style3.setDataFormat(dm.getFormat("@"));
		
		XSSFCellStyle[] styleArray = new XSSFCellStyle[3];
		styleArray[0]=style1;
		styleArray[1]=style2;
		styleArray[2]=style3;
		return styleArray;
	}
	
	public void exportFileTo(File filename){
		try{
			FileOutputStream fos = new FileOutputStream(filename);
			workbook.write(fos);
			fos.close();
			System.out.println("Berhasil");
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
