package nukhy.project.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import nukhy.project.Data.DataPajakMasuk;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelExporterPajakMasukan {
	private Workbook workbook;
	private Sheet fpSheet;
	private ArrayList<DataPajakMasuk> data;
	public static final String[][] title=
		{{"FM","KD_JENIS_TRANSAKSI","FG_PENGGANTI","NOMOR_FAKTUR","MASA_PAJAK","TAHUN_PAJAK","TANGGAL_PAJAK","NPWP","NAMA","ALAMAT_LENGKAP","JUMLAH_DPP",
			"JUMLAH_PPN","JUMLAH_PPNBM","IS_CREDITABLE"}
		};
	public ExcelExporterPajakMasukan(ArrayList<DataPajakMasuk> dt){
		workbook = new XSSFWorkbook();
		fpSheet = workbook.createSheet("Export FP");
		this.data = dt;
		System.out.println("Buat Header...");
		createHeader();
		System.out.println("Buat Content...");
		createContent();
	}
	
	public void createHeader(){
		Row row;
		int rowIndex=0;
		XSSFCellStyle[] style=headerStyle();
		for(int cell=0;cell<title.length;cell++){
			row = fpSheet.createRow(rowIndex++);
			for(int column=0;column<title[0].length;column++){
				if(column>=title[cell].length){
					row.createCell(column).setCellValue("");
					row.getCell(column).setCellStyle(style[cell]);
					continue;
				}
				row.createCell(column).setCellValue(title[cell][column]);
				row.getCell(column).setCellStyle(style[cell]);
			}
		}	
	}
	
	public void createContent(){
		Row row;
		int rowIndex=1;
		XSSFCellStyle[] style = headerStyle();
		int i=0;
		for(DataPajakMasuk sdata:data){
			if(!sdata.exportData) continue;
			System.out.println("Masukin Data "+ i++);
			row = fpSheet.createRow(rowIndex++);
			Object[] fk = sdata.of.getArrayObject();
			for(int column=1;column<fk.length;column++){
				row.createCell(column-1).setCellValue(fk[column].toString());
				row.getCell(column-1).setCellStyle(style[0]);
				row.getCell(column-1).setCellType(Cell.CELL_TYPE_STRING);
			}
		}
		autoSizeColumn();
	}
	
	public void autoSizeColumn(){
		for(int i=0;i<19;i++){
			fpSheet.autoSizeColumn(i);
		}
	}
	
	public XSSFCellStyle[] headerStyle(){
		Font font = workbook.createFont();
		//font.setBold(true);
		DataFormat dm = workbook.createDataFormat();
		XSSFCellStyle style1 = (XSSFCellStyle) workbook.createCellStyle();
		style1.setFont(font);
		style1.setDataFormat(dm.getFormat("@"));
		
		XSSFCellStyle[] styleArray = new XSSFCellStyle[1];
		styleArray[0]=style1;
		return styleArray;
	}
	
	public void exportFileTo(File filename){
		try{
			FileOutputStream fos = new FileOutputStream(filename);
			workbook.write(fos);
			fos.close();
			System.out.println("Berhasil");
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
