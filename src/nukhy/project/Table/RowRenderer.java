package nukhy.project.Table;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


public class RowRenderer extends DefaultTableCellRenderer{
	private static final long serialVersionUID = -1606069160866643041L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		// TODO Auto-generated method stub
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
		if(value instanceof Boolean){
	           if(value != null)
	            {
	                 JCheckBox jcb = new JCheckBox();
	                 jcb.setSelected((Boolean) value);

	                 return jcb;
	            }
	            //return new JPanel();
		}
		String type = (String) table.getValueAt(row, 1);
		if(type.equals("FK")){
			setBackground(new Color(0xD8E4BC));
		}else if(type.equals("LT")){
			setBackground(new Color(0xFCD5B4));
		}else if(type.equals("OF")){
			setBackground(new Color(0xFFFF00));
		}else{
			setBackground(new Color(0xFFFFFF));
		}
		
		if(table.isCellSelected(row, column)){
			setBackground(new Color(0x3498DB));
		}
		return this;
	}

}
