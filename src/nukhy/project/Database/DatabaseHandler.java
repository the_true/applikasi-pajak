package nukhy.project.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import nukhy.project.Data.DataPajakKeluar;
import nukhy.project.Data.DataPajakMasuk;
import nukhy.project.ItemData.FKItem;
import nukhy.project.ItemData.FMItem;
import nukhy.project.ItemData.LTItem;
import nukhy.project.ItemData.OFItem;

public class DatabaseHandler {
	public Connection con = null;
	private String connectionUrl="";
	public ArrayList<DataPajakKeluar> dataKeluaran;
	public ArrayList<DataPajakMasuk> dataMasukan;
	private String username,password,database,port;
	public int totalSum=0;
	public DatabaseHandler(String username,String password){
		this.username=username;
		this.password=password;
		this.database="";
		this.port="1433";
	}
	
	public String loginDB(){
		this.connectionUrl="jdbc:sqlserver://localhost:"+port+";"
				+"username="+username+";password="+password+";"
				+"databaseName="+database+";";
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con=DriverManager.getConnection(connectionUrl);
			//System.out.println("Connected");
		}catch(Exception e){
			e.printStackTrace();
			authDB();
		}		
		dataKeluaran = new ArrayList<DataPajakKeluar>();
		dataMasukan = new ArrayList<DataPajakMasuk>();
		return "Login Berhasil";
	}
	
	public void closeCon(){
		if(con!=null) try {	con.close();} catch (SQLException e) {}
	}
	
	public String authDB(){
		this.connectionUrl="jdbc:sqlserver://localhost:"+port+";"
				+"username="+username+";password="+password+";";
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con=DriverManager.getConnection(connectionUrl);
			//System.out.println("Connected");
		}catch(Exception e){
			return e.getMessage();
			//return "Gagal";
		}
		listDB();
		return "Berhasil";
	}
	
	public void getDataPajakKeluaran(String date){
		String query = "WHERE MONTH(BD.BILL_DATE) = MONTH('"+date+"') AND YEAR(BD.BILL_DATE) = YEAR('"+date+"')";
		this.executePajakKeluaran(query);
	}
	
	public void getDataPajakMasukan(String from, String to){
		String query = "WHERE B.BILL_DATE BETWEEN '"+from+"' AND '"+to+"' AND B.CFORM_NO NOT LIKE ''";
		this.executePajakMasukan(query);
		query = "WHERE B.BILL_DATE BETWEEN '"+from+"' AND '"+to+"' AND B.CFORM_NO LIKE ''";
		this.executePajakMasukan(query, true);
	}
	
	public void getDataPajakMasukan(String invoiceNo, int invo){
		String[] invoice = invoiceNo.split(",");
		if(invoice.length<=0) return;
		String CFORM_NO = invoice[0];
		if(!invoice[0].contains(".")){
			CFORM_NO = "010."+invoice[0].substring(0, 3)+"-"+invoice[0].substring(3,5)+"."+invoice[0].substring(5);
		}
		String condition = "B.CFORM_NO='"+CFORM_NO+"'";
		for(int i=1;i<invoice.length;i++){
			//010.002-15.53688789
			CFORM_NO = invoice[i];
			if(!invoice[i].contains(".")){
				CFORM_NO = "010."+invoice[i].substring(0, 3)+"-"+invoice[i].substring(3,5)+"."+invoice[i].substring(5);
			}
			condition+=" OR B.CFORM_NO ='"+invoice[i]+"' ";
		}
		String query = "WHERE "+condition;
		this.executePajakMasukan(query);
	}
	
	public void getDataPajakKeluaran(String from, String to){
		String query = "WHERE BD.BILL_DATE BETWEEN '"+from+"' AND '"+to+"'";
		this.executePajakKeluaran(query);
	}
	
	public void getDataPajakKeluaran(String invoiceNo, int invo){
		String[] invoice = invoiceNo.split(",");
		String condition = "B.BILL_NO='"+invoice[0]+"'";
		for(int i=1;i<invoice.length;i++){
			condition+=" OR B.BILL_NO ='"+invoice[i]+"' ";
		}
		String query = "WHERE "+condition;
		this.executePajakKeluaran(query);
	}
	
	public void executePajakKeluaran(String query){
		dataKeluaran = new ArrayList<DataPajakKeluar>();
		Statement stmt=null;
		ResultSet rs=null;
		try{
			String SQL = "SELECT CAST(BD.RATE as numeric(10,0)) AS RATE,CAST(BD.QUANTITY as numeric(10,0)) AS QUANTITY,CAST(BD.AMOUNT as numeric(10,0)) AS AMOUNT,CAST(BD.AMOUNT2 as numeric(10,0)) AS AMOUNT2,CAST(BD.DISCOUNT as numeric(10,0)) AS DISCOUNT,CAST(BD.DEDUCTION as numeric(10,0)) AS DEDUCTION,"
					+ "B.CFORM_NO,convert(varchar, B.BILL_DATE, 103) as BILL_DATE2, B.BILL_DATE,CAST(B.BASIC as numeric(10,0)) AS BASIC,CAST(B.ADDITION as numeric(10,0)) AS ADDITION,RTRIM(B.BILL_NO) AS BILL_NO,"
					+ "C.CST_NO,C.NAME,C.ADDRESS1,RTRIM(C.ADDRESS2) AS ADDRESS2,RTRIM(C.ADDRESS3) AS ADDRESS3,"
					+ "P.DESCRIPT, P.PRODCODE "
					+ "FROM BILLDET AS BD "
					+ "LEFT JOIN BILL AS B ON BD.BILL_NO = B.BILL_NO "
					+ "LEFT JOIN PRODUCT AS P ON BD.PRODCODE=P.PRODCODE "
					+ "LEFT JOIN CUSTOMER AS C ON BD.CUSTCODE = C.CUSTCODE "
					+ query
					+ "ORDER BY B.BILL_DATE,B.BILL_NO,BD.ORDERSEQ";
			System.out.println(SQL);
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL);
			String last_bill_no="";
			FKItem fktemp=null;
			LTItem lttemp=null;
			ArrayList<OFItem> ofdata=null;
			while (rs.next()) {
				if(!last_bill_no.equals(rs.getString("BILL_NO"))){
					if(fktemp!=null){
						dataKeluaran.add(new DataPajakKeluar(fktemp, lttemp, ofdata));
					}
					Calendar cal = Calendar.getInstance();
					cal.setTime(rs.getDate("BILL_DATE"));
					int month = cal.get(Calendar.MONTH)+1;
					int year  = cal.get(Calendar.YEAR);
					String nomer_faktur = rs.getString("CFORM_NO").substring(3).replaceAll("[^a-zA-Z0-9]", "");
					//if (nomer_faktur.equals("")) nomer_faktur.replace(oldChar, newChar)
					String npwp = rs.getString("CST_NO").replaceAll("[^a-zA-Z0-9]", "");
					fktemp = new FKItem("01", "0", 
							nomer_faktur, Integer.toString(month), Integer.toString(year), 
							rs.getString("BILL_DATE2"), npwp, rs.getString("ADDRESS1"), rs.getString("ADDRESS2")+" "+rs.getString("ADDRESS3"), 
							rs.getString("BASIC"), rs.getString("ADDITION"), "0", "0", 
							"0", "0", "0", "0", "INVOICE NO. "+rs.getString("BILL_NO"));
					lttemp = new LTItem(npwp, rs.getString("ADDRESS1"), "JALAN", "BLOK", "0", 
							"0", "0", "KECAMATAN", "KELURAHAN", "KABUPATEN", 
							"PROPINSI", "", "");
					ofdata = new ArrayList<OFItem>();
					last_bill_no = rs.getString("BILL_NO");
				}
				int dpp = rs.getInt("AMOUNT2")-rs.getInt("DEDUCTION");
				int ppn = (int) Math.floor(10*dpp/100);
				ofdata.add(new OFItem("", rs.getString("DESCRIPT"), rs.getString("RATE"), 
						rs.getString("QUANTITY"), rs.getString("AMOUNT"), rs.getString("DEDUCTION"), 
						Integer.toString(dpp), Integer.toString(ppn), "0","0"));
			}
			if(fktemp!=null){
				dataKeluaran.add(new DataPajakKeluar(fktemp, lttemp, ofdata));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (rs != null) try { rs.close(); } catch(Exception e) {}
    		if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
		totalSum=0;
		for(DataPajakKeluar datasingle: dataKeluaran){
			int totalDPP = 0;
			int totalPPN = 0;
			for(OFItem ofdata: datasingle.of){
				totalDPP+=(Integer.parseInt(ofdata.dpp));
				totalSum+=(Integer.parseInt(ofdata.dpp));
				totalPPN+=(10*(Integer.parseInt(ofdata.dpp))/100);
			}
			datasingle.fk.jumlah_dpp=Integer.toString(totalDPP);
			int selisih = Integer.parseInt(datasingle.fk.jumlah_ppn)-totalPPN;
			if(selisih==0) continue;
			//System.out.print(datasingle.fk.referensi+" selisih "+ selisih);
			for(OFItem ofdata: datasingle.of){
				if(selisih==0)break;
				int nominalterakhir = Integer.parseInt(ofdata.dpp.substring(ofdata.dpp.length()-1));
				if(nominalterakhir>=4){
					int ppnmod = Integer.parseInt(ofdata.ppn)+1;
					ofdata.ppn=Integer.toString(ppnmod);
					selisih--;
				}
			}
		}
	}
	
	public void executePajakMasukan(String query){
		totalSum=0;
		dataMasukan = new ArrayList<DataPajakMasuk>();
		Statement stmt = null;
		ResultSet rs = null;
		try{
			String SQL = "SELECT B.CFORM_NO,convert(varchar, MAX(B.P_BILLDATE), 103) as P_BILLDATE2,"
					+ "MAX(B.BILL_DATE) as P_BILLDATE,MAX(B.P_BILLDATE) as P_BILLDATE3,CAST(SUM(BD.AMOUNT) as numeric(10,0)) AS AMOUNT,"
					+ "MAX(C.CST_NO) AS CST_NO, MAX(C.NAME) as NAME,CAST(SUM(BT.AMOUNT) as numeric(10,0)) as TAX "
					+ "FROM BLLINDET AS BD "
					+ "LEFT JOIN CUSTOMER AS C ON BD.CUSTCODE=C.CUSTCODE "
					+ "LEFT JOIN BILLIN AS B ON BD.BILL_NO=B.BILL_NO "
					+ "LEFT JOIN BLINTERM AS BT ON BD.BILL_NO=BT.BILL_NO "
					+ query+" AND "
					+ "BD.AMOUNT=BD.AMOUNT2 AND BT.AMOUNT>'0' "
					+ "GROUP BY CFORM_NO "
					+ "ORDER BY MAX(B.P_BILLDATE)";
			System.out.println(SQL);
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL);
			while (rs.next()) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(rs.getDate("P_BILLDATE"));
				int month = cal.get(Calendar.MONTH)+1;
				int year  = cal.get(Calendar.YEAR);
				cal.setTime(rs.getDate("P_BILLDATE3"));
				int yearpajak = cal.get(Calendar.YEAR);
				String tanggal_pajak="";
				if(yearpajak>2000){
					tanggal_pajak=rs.getString("P_BILLDATE2");
				}
				String npwp = rs.getString("CST_NO").replaceAll("[^a-zA-Z0-9]", "");
				String nomer_faktur = rs.getString("CFORM_NO").substring(3).replaceAll("[^a-zA-Z0-9]", "");
				//int ppn = (int) Math.floor(10*rs.getInt("AMOUNT")/100);
				dataMasukan.add(new DataPajakMasuk(new FMItem("01", "0", nomer_faktur, Integer.toString(month), 
						Integer.toString(year), tanggal_pajak, npwp, rs.getString("NAME"), "", rs.getString("AMOUNT"), 
						rs.getString("TAX"), "0", "1")));
				totalSum+=((rs.getInt("AMOUNT")));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (rs != null) try { rs.close(); } catch(Exception e) {}
    		if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
				
	}
	
	public void executePajakMasukan(String query,boolean nogrouping){
		Statement stmt = null;
		ResultSet rs = null;
		try{
			String SQL = "SELECT B.CFORM_NO,convert(varchar, B.P_BILLDATE, 103) as P_BILLDATE2,"
					+ "B.BILL_DATE as P_BILLDATE,B.P_BILLDATE as P_BILLDATE3,CAST(BD.AMOUNT as numeric(10,0)) AS AMOUNT,"
					+ "C.CST_NO AS CST_NO, C.NAME as NAME,CAST(BT.AMOUNT as numeric(10,0)) as TAX "
					+ "FROM BLLINDET AS BD "
					+ "LEFT JOIN CUSTOMER AS C ON BD.CUSTCODE=C.CUSTCODE "
					+ "LEFT JOIN BILLIN AS B ON BD.BILL_NO=B.BILL_NO "
					+ "LEFT JOIN BLINTERM AS BT ON BD.BILL_NO=BT.BILL_NO "
					+ query+" AND "
					+ "BD.AMOUNT=BD.AMOUNT2 AND BT.AMOUNT>'0' "
					+ "ORDER BY B.P_BILLDATE";
			System.out.println(SQL);
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL);
			while (rs.next()) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(rs.getDate("P_BILLDATE"));
				int month = cal.get(Calendar.MONTH)+1;
				int year  = cal.get(Calendar.YEAR);
				cal.setTime(rs.getDate("P_BILLDATE3"));
				int yearpajak = cal.get(Calendar.YEAR);
				String tanggal_pajak="";
				if(yearpajak>2000){
					tanggal_pajak=rs.getString("P_BILLDATE2");
				}
				String npwp = rs.getString("CST_NO").replaceAll("[^a-zA-Z0-9]", "");
				String nomer_faktur = rs.getString("CFORM_NO").substring(3).replaceAll("[^a-zA-Z0-9]", "");
				//int ppn = (int) Math.floor(10*rs.getInt("AMOUNT")/100);
				dataMasukan.add(new DataPajakMasuk(new FMItem("01", "0", nomer_faktur, Integer.toString(month), 
						Integer.toString(year), tanggal_pajak, npwp, rs.getString("NAME"), "", rs.getString("AMOUNT"), 
						rs.getString("TAX"), "0", "1")));
				totalSum+=((rs.getInt("AMOUNT")));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (rs != null) try { rs.close(); } catch(Exception e) {}
    		if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
				
	}
	
	public ArrayList<String> listDB(){
		ArrayList<String> dbList= new ArrayList<String>();
		Statement stmt=null;
		ResultSet rs=null;
		String SQL = "SELECT RTRIM(name) as name FROM sys.databases d WHERE d.database_id > 4";
		try{
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL);
			while(rs.next()){
				dbList.add(rs.getString(1));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (rs != null) try { rs.close(); } catch(Exception e) {}
    		if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		}
		return dbList;
	}

	public String getCurrentDB() {
		return database;
	}
	
	public void setCurrentDB(String database){
		this.database = database;
	}

	public String getPort() {
		return port;
	}

}
