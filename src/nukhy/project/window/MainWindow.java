package nukhy.project.window;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import nukhy.project.Data.DataPajakKeluar;
import nukhy.project.Data.DataPajakMasuk;
import nukhy.project.Database.DatabaseHandler;
import nukhy.project.ItemData.OFItem;
import nukhy.project.Table.RowRenderer;
import nukhy.project.excel.ExcelExporterPajakKeluaran;
import nukhy.project.excel.ExcelExporterPajakMasukan;
import nukhy.project.properties.PropertiesFile;

import com.toedter.calendar.JDateChooser;

import javax.swing.JRadioButtonMenuItem;

public class MainWindow {
	public JFrame frame;
	public JTable table;
	private JFileChooser saveTo;
	private DatabaseHandler handler;
	private JDateChooser fromChooser,toChooser;
	private PropertiesFile prop;
	private JLabel loadingLabel,lblDatabase;
	private JButton btnSubmit,btnExportToExcel;
	private JPanel rangePanel,invoicePanel;
	private JRadioButton rdbtnRange,rdbtnInvoice;
	private JTextArea invoiceTxt;
	private JLabel label;
	private JLabel lblUnselectAll,lblSelectAll;
	private JLabel totalSum;
	private JRadioButtonMenuItem rdbtnmntmPajakKeluaran;
	private JRadioButtonMenuItem rdbtnmntmPajakMasukan;
	public MainWindow() {
		//exporter=new ExcelExporter();
		initialize();
		prop = new PropertiesFile();
	}
	
	public void setLoginAuth(String username, String password){
		handler=new DatabaseHandler(username,password);
		handler.setCurrentDB(prop.getDatabase());
		handler.loginDB();
		lblDatabase.setText("  Database : "+handler.getCurrentDB());
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Xporter");
		frame.setBounds(100, 100, 859, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width/2-frame.getSize().width/2, 5);
		frame.addWindowListener(new WindowAdapter()
		{
		    public void windowClosing(WindowEvent e)
		    {
		    	handler.closeCon();
		    }
		});
		saveTo = new JFileChooser();
		ImageIcon loader = new ImageIcon("icon/loading-big.gif");
		ImageIcon icon = new ImageIcon("icon/icon.png");
		frame.setIconImage(icon.getImage());
		
		btnSubmit = new JButton("Search");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadingLabel.setVisible(true);
				btnSubmit.setEnabled(false);
				Thread db = new PopulateTable();
				db.start();
			}
		});
		btnSubmit.setBounds(323, 75, 89, 23);
		frame.getContentPane().add(btnSubmit);
		
		btnExportToExcel = new JButton("Export to Excel");
		btnExportToExcel.setBounds(711, 45, 132, 23);
		frame.getContentPane().add(btnExportToExcel);
		btnExportToExcel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = saveTo.showSaveDialog(frame);
				   if (result == JFileChooser.APPROVE_OPTION) {
				        try {
				        	loadingLabel.setVisible(true);
				        	File file = saveTo.getSelectedFile();
				        	if(!getFileExtension(file).toLowerCase().equals(".xlsx")){
				        		file = new File(saveTo.getSelectedFile().getAbsolutePath()+".xlsx");
				        	}
				        	btnExportToExcel.setEnabled(false);
							Thread ex = new ExportExcel(file);
							ex.start();
				        } catch (Exception ex) {
				            ex.printStackTrace();
				        }
			    }
			}
		});
		
		table = new JTable() {
			private static final long serialVersionUID = 5847511704643029654L;
			public boolean getScrollableTracksViewportWidth() {
				return getRowCount() == 0 ? super
						.getScrollableTracksViewportWidth()
						: getPreferredSize().width < getParent().getWidth();
			}
		};
		//table.setIntercellSpacing(new Dimension(10, 5));
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(10, 134, 833, 527);
		frame.getContentPane().add(scrollPane);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 853, 21);
		frame.getContentPane().add(menuBar);
		
		JMenu File = new JMenu("File");
		menuBar.add(File);
		
		JMenuItem mnitemDb = new JMenuItem("Select Database...");
		File.add(mnitemDb);
		mnitemDb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Options dialog = new Options(handler.listDB(),handler.getPort(),handler.getCurrentDB());
				dialog.setLocationRelativeTo(frame);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);
				String databasenya = dialog.currentDb;
				handler.setCurrentDB(databasenya);
				prop.saveParamChanges(databasenya);
				handler.loginDB();
				lblDatabase.setText("  Database : "+databasenya);
			}
		});
		
		JMenuItem mnitemExport = new JMenuItem("Export to Excel...");
		File.add(mnitemExport);
		mnitemExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = saveTo.showSaveDialog(frame);
				   if (result == JFileChooser.APPROVE_OPTION) {
				        try {
				        	File file = saveTo.getSelectedFile();
				        	if(!getFileExtension(file).toLowerCase().equals(".xlsx")){
				        		loadingLabel.setVisible(true);
				        		file = new File(saveTo.getSelectedFile().getAbsolutePath()+".xlsx");
								Thread ex = new ExportExcel(file);
								ex.start();
								
				        	}

				        } catch (Exception ex) {
				            ex.printStackTrace();
				        }
			    }
			}
		});
		
		JMenuItem mnitemAbout = new JMenuItem("About");
		File.add(mnitemAbout);
		mnitemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, 
						"<html><body><p style='width: 200px;'>"
						+ "<table width='100%' cellspacing='0'> <tbody><tr> <td>&nbsp;&nbsp;&nbsp;</td> <td colspan='5'> <span style='font-family:Arial, Helvetica, sans-serif; font-size:25px; color:#FFF'><u><b>Support</b></u></span> <br> <span style='font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#CCC'><i>For urgent inquiries, please contact us by phone or send us an email.</i></span><br><br> </td> </tr> <tr> <td style='border-bottom:1px #CCC solid'>&nbsp;&nbsp;&nbsp;</td> <td style='border-bottom:1px #CCC solid'> <span style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#CCC'>Ajuna Kaliantiga</span><br> <span style='font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#0CF'>Developer</span> </td> <td style='border-bottom:1px #CCC solid'>&nbsp;&nbsp;&nbsp;</td> <td style='border-bottom:1px #CCC solid'> <span style='font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF'>+6281294294564</span> </td> <td style='border-bottom:1px #CCC solid'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> <td style='border-bottom:1px #CCC solid'> <a href='mailto:bagoes.cahyono@wicelltechnologies.com' style='text-decoration:none; color:#FFF'> <span style='font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF'>ajuna182@gmail.com</span></a> </td> </tr> <tr><td>&nbsp;</td></tr> <tr> <td style='border-bottom:1px #CCC solid'>&nbsp;&nbsp;&nbsp;</td> <td style='border-bottom:1px #CCC solid'> <span style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#CCC'>Nukhy Nugroho</span><br> <span style='font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#0CF'>Developer</span> </td> <td style='border-bottom:1px #CCC solid'>&nbsp;&nbsp;&nbsp;</td> <td style='border-bottom:1px #CCC solid'> <span style='font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF'>+6281295091028</span> </td> <td style='border-bottom:1px #CCC solid'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> <td style='border-bottom:1px #CCC solid'> <a href='mailto:nukhy.nugroho@wicelltechnologies.com' style='text-decoration:none; color:#FFF'> <span style='font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF'>nukhyrn@gmail.com</span></a> </td> </tr> </tbody></table>"
						+ ""
						+ "</p></body></html>", 
					    "ERROR",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		JMenuItem mnitemExit = new JMenuItem("Exit");
		File.add(mnitemExit);
		
		JMenu mnReport = new JMenu("Report");
		menuBar.add(mnReport);
		
		rdbtnmntmPajakKeluaran = new JRadioButtonMenuItem("Pajak Keluaran");
		rdbtnmntmPajakKeluaran.setSelected(true);
		mnReport.add(rdbtnmntmPajakKeluaran);
		rdbtnmntmPajakKeluaran.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createTable();
			}
		});
		
		rdbtnmntmPajakMasukan = new JRadioButtonMenuItem("Pajak Masukan");
		mnReport.add(rdbtnmntmPajakMasukan);
		rdbtnmntmPajakMasukan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createTable();
			}
		});
		
		ButtonGroup menuRadBtnGroup = new ButtonGroup();
		menuRadBtnGroup.add(rdbtnmntmPajakMasukan);
		menuRadBtnGroup.add(rdbtnmntmPajakKeluaran);

		mnitemExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog(frame, "Do you really want to exit?", "Exit", dialogButton);
				if(dialogResult == 0) {
				  frame.dispose();
				}
			}
		});
		
		lblDatabase = new JLabel("");
		lblDatabase.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDatabase.setBackground(Color.LIGHT_GRAY);
		lblDatabase.setOpaque(true);
		lblDatabase.setBounds(0, 19, 853, 20);
		frame.getContentPane().add(lblDatabase);
		
		loadingLabel = new JLabel(loader);
		loadingLabel.setBounds(0, 29, 853, 14);
		loadingLabel.setVisible(false);
		frame.getContentPane().add(loadingLabel);
		
		rangePanel = new JPanel();
		rangePanel.setBounds(10, 59, 309, 53);
		frame.getContentPane().add(rangePanel);
		rangePanel.setLayout(null);
		
		invoicePanel = new JPanel();
		invoicePanel.setBounds(10, 59, 309, 53);
		frame.getContentPane().add(invoicePanel);
		invoicePanel.setLayout(null);
		invoicePanel.setVisible(false);
		
		JLabel lblInvoiceNo = new JLabel("Invoice no:");
		lblInvoiceNo.setBounds(10, 5, 69, 14);
		invoicePanel.add(lblInvoiceNo);
		
		invoiceTxt = new JTextArea();
		invoiceTxt.setLineWrap(true);
		invoiceTxt.setBounds(106, 5, 200, 45);
		Border border = BorderFactory.createLineBorder(Color.GRAY);
		invoiceTxt.setBorder(BorderFactory.createCompoundBorder(border, 
		            BorderFactory.createEmptyBorder(1, 1, 1, 1)));
		//invoicePanel.add(invoiceTxt);
		invoiceTxt.setColumns(10);
		
		JScrollPane invoiceScroll = new JScrollPane(invoiceTxt);
		invoiceScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		invoiceScroll.setBounds(106, 5, 200, 45);
		invoicePanel.add(invoiceScroll);
		
		JLabel lblNewLabel = new JLabel("Date from:");
		lblNewLabel.setBounds(10, 5, 59, 14);
		rangePanel.add(lblNewLabel);
		
		
		fromChooser = new JDateChooser();
		fromChooser.setBounds(106, 5, 147, 20);
		rangePanel.add(fromChooser);
		fromChooser.setDateFormatString("dd/MM/yyyy");
		
		toChooser = new JDateChooser();
		toChooser.setBounds(106, 30, 147, 20);
		rangePanel.add(toChooser);
		toChooser.setDateFormatString("dd/MM/yyyy");
		
		JLabel lblDateTo = new JLabel("Date to    :");
		lblDateTo.setBounds(10, 30, 59, 14);
		rangePanel.add(lblDateTo);

		
		JLabel lblFilterBy = new JLabel("Filter by   :");
		lblFilterBy.setBounds(21, 43, 69, 14);
		frame.getContentPane().add(lblFilterBy);
		
		rdbtnRange = new JRadioButton("Date");
		rdbtnRange.setBounds(96, 39, 54, 23);
		rdbtnRange.setSelected(true);
		rdbtnRange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rangePanel.setVisible(true);
				invoicePanel.setVisible(false);
			}
		});
		frame.getContentPane().add(rdbtnRange);
		
		rdbtnInvoice = new JRadioButton("Invoice");
		rdbtnInvoice.setBounds(170, 39, 109, 23);
		frame.getContentPane().add(rdbtnInvoice);
		rdbtnInvoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				invoicePanel.setVisible(true);
				rangePanel.setVisible(false);
			}
		});
		
		
		createTable();
		
		ButtonGroup radBtnGroup = new ButtonGroup();
		radBtnGroup.add(rdbtnRange);
		radBtnGroup.add(rdbtnInvoice);
		
		lblSelectAll = new JLabel("Select All");
		lblSelectAll.setForeground (Color.BLUE);
		lblSelectAll.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblSelectAll.setBounds(10, 120, 46, 14);
		lblSelectAll.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setSelect(true);
			}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
		});
		frame.getContentPane().add(lblSelectAll);
		
		label = new JLabel("/");
		label.setBounds(56, 119, 13, 14);
		frame.getContentPane().add(label);
		
		lblUnselectAll = new JLabel("Unselect All");
		lblUnselectAll.setForeground (Color.BLUE);
		lblUnselectAll.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblUnselectAll.setBounds(64, 120, 62, 14);
		lblUnselectAll.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setSelect(false);
			}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
		});
		frame.getContentPane().add(lblUnselectAll);
		
		totalSum = new JLabel("<html><body><span style='font-size:16px;font-weight: BOLD;'> Total Sum </span> : <span style='font-size:12px;'>RP. 0</span></body></html>");
		totalSum.setVerticalAlignment(SwingConstants.BOTTOM);
		totalSum.setBounds(587, 79, 256, 53);
		frame.getContentPane().add(totalSum);
	}
	
	public void createTable(){
		CustomeModel model = new CustomeModel();
		Object[][] title = null;
		if(rdbtnmntmPajakMasukan.isSelected()){
			title = DataPajakMasuk.title;
		}else{
			title = DataPajakKeluar.title;
		}
		model.setColumnCount(title[0].length);
		for(int i=0;i<title.length;i++)model.addRow(title[i]);
		table.setModel(model);
		table.setDefaultRenderer(Object.class, new RowRenderer());
		table.setDefaultRenderer(Boolean.class, new RowRenderer());
		for(int i=0;i<title[0].length;i++){
			fitToContentWidth(table, i);
		}
	}
	
	public void insertDataPajakKeluaran(){
		CustomeModel model = (CustomeModel) table.getModel();
		for(int i=0;i<handler.dataKeluaran.size();i++){
			model.addRow(handler.dataKeluaran.get(i).fk.getArrayObject());
			model.addRow(handler.dataKeluaran.get(i).lt.getArrayObject());
			for(OFItem ofnya: handler.dataKeluaran.get(i).of){
				model.addRow(ofnya.getArrayObject());
			}
		}
		table.setModel(model);
		for(int i=1;i<20;i++){
			fitToContentWidth(table, i);
		}
	}
	
	public void insertDataPajakMasukan(){
		CustomeModel model = (CustomeModel) table.getModel();
		int size = DataPajakMasuk.title[0].length;
		for(int i=0;i<handler.dataMasukan.size();i++){
			model.addRow(handler.dataMasukan.get(i).of.getArrayObject());
		}
		table.setModel(model);
		for(int i=1;i<size;i++){
			fitToContentWidth(table, i);
		}
	}
	
	public static void fitToContentWidth(final JTable table, final int column) {
		int width = 0;
		for (int row = 0; row < table.getRowCount(); ++row) {
			final Object cellValue = table.getValueAt(row, column);
			final TableCellRenderer renderer = table.getCellRenderer(row,
					column);
			final Component comp = renderer.getTableCellRendererComponent(
					table, cellValue, false, false, row, column);
			width = Math.max(width, comp.getPreferredSize().width);
		}
		final TableColumn tc = table.getColumn(table.getColumnName(column));
		width += table.getIntercellSpacing().width * 2+20;
		tc.setPreferredWidth(width);
		tc.setMinWidth(width);
	}
	
	public void resizeColumnWidth(JTable table) {
	    final TableColumnModel columnModel = table.getColumnModel();
	    for (int column = 0; column < table.getColumnCount(); column++) {
	        int width = 50; // Min width
	        for (int row = 0; row < table.getRowCount(); row++) {
	            TableCellRenderer renderer = table.getCellRenderer(row, column);
	            Component comp = table.prepareRenderer(renderer, row, column);
	            width = Math.max(comp.getPreferredSize().width +1 , width);
	        }
	        columnModel.getColumn(column).setPreferredWidth(width);
	    }
	}
	
	public void setSelect(Boolean select){
		CustomeModel dtm = (CustomeModel) table.getModel();
	    int nRow = dtm.getRowCount();
	    for (int i = 0 ; i < nRow ; i++){
	    	Boolean doExport = (Boolean) dtm.getValueAt(i,0);
	    	if(doExport == null) continue;
	    	dtm.setValueAt(select, i, 0);
	    }
	}
	
	public void setExportRowKeluaran () {
		CustomeModel dtm = (CustomeModel) table.getModel();
	    int nRow = dtm.getRowCount();
	    int inotnull=0;
	    for (int i = 0 ; i < nRow ; i++){
	    	Boolean doExport = (Boolean) dtm.getValueAt(i,0);
	    	if(doExport == null) continue;
	    	DataPajakKeluar item = handler.dataKeluaran.get(inotnull);
	    	item.exportData=doExport;
	    	inotnull++;
	    }
	}
	
	public void setExportRowMasukan(){
		CustomeModel dtm = (CustomeModel) table.getModel();
	    int nRow = dtm.getRowCount();
	    int inotnull=0;
	    for (int i = 0 ; i < nRow ; i++){
	    	Boolean doExport = (Boolean) dtm.getValueAt(i,0);
	    	if(doExport == null) continue;
	    	DataPajakMasuk item = handler.dataMasukan.get(inotnull);
	    	item.exportData=doExport;
	    	inotnull++;
	    }
	}
	
	private String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf("."));

	    } catch (Exception e) {
	        return "";
	    }

	}
	
	class PopulateTable extends Thread{
		public void run(){
			if(handler.getCurrentDB()==null || handler.getCurrentDB()==""){
				JOptionPane.showMessageDialog(frame, "<html><body><p style='width: 200px;'> Please Select Database</p></body></html>", 
					    "ERROR",JOptionPane.ERROR_MESSAGE);
			}
			try {
				sleep(1000);
			} catch (InterruptedException e) {

			}
			createTable();
			loadingLabel.setVisible(false);
			btnSubmit.setEnabled(true);
			if(rdbtnmntmPajakKeluaran.isSelected()){
				if(rdbtnRange.isSelected()){
					handler.getDataPajakKeluaran(new SimpleDateFormat("yyyy-MM-dd").format(fromChooser.getDate()),new SimpleDateFormat("yyyy-MM-dd").format(toChooser.getDate()));
				}else if(rdbtnInvoice.isSelected()){
					handler.getDataPajakKeluaran(invoiceTxt.getText(), 1);
				}
				insertDataPajakKeluaran();
				totalSum.setText("<html><body><span style='font-size:16px;font-weight: BOLD;'> Total Sum </span> : <span style='font-size:12px;'>RP. "+handler.totalSum+"</span></body></html>");
			}else{
				if(rdbtnRange.isSelected()){
					handler.getDataPajakMasukan(new SimpleDateFormat("yyyy-MM-dd").format(fromChooser.getDate()),new SimpleDateFormat("yyyy-MM-dd").format(toChooser.getDate()));
				}else if(rdbtnInvoice.isSelected()){
					handler.getDataPajakKeluaran(invoiceTxt.getText(), 1);
				}	
				insertDataPajakMasukan();
				totalSum.setText("<html><body><span style='font-size:16px;font-weight: BOLD;'> Total Sum </span> : <span style='font-size:12px;'>RP. "+handler.totalSum+"</span></body></html>");
			}
			return;
		}
	}
	
	class ExportExcel extends Thread{
		private File file;
		public ExportExcel(File file){
			this.file=file;
		}
		public void run(){
        	System.out.println("Mulai Export Data");
        	if(rdbtnmntmPajakKeluaran.isSelected()){
	        	setExportRowKeluaran();
	        	ExcelExporterPajakKeluaran exporter = new ExcelExporterPajakKeluaran(handler.dataKeluaran);
	        	System.out.println("Content dibuat tinggal export");
	        	exporter.exportFileTo(file);
	        	System.out.println("Export Data");
	        	loadingLabel.setVisible(false);
	        	btnExportToExcel.setEnabled(true);
	        	JOptionPane.showMessageDialog(frame, "Successfully exporting to excel", "Exit", JOptionPane.INFORMATION_MESSAGE);
        	}else{
	        	setExportRowMasukan();
	        	ExcelExporterPajakMasukan exporter = new ExcelExporterPajakMasukan(handler.dataMasukan);
	        	System.out.println("Content dibuat tinggal export");
	        	exporter.exportFileTo(file);
	        	System.out.println("Export Data");
	        	loadingLabel.setVisible(false);
	        	btnExportToExcel.setEnabled(true);
	        	JOptionPane.showMessageDialog(frame, "Successfully exporting to excel", "Exit", JOptionPane.INFORMATION_MESSAGE);
        	}
		}
	}
	
	public class CustomeModel extends DefaultTableModel {
		private static final long serialVersionUID = -4184925189780050302L;

		public CustomeModel() {
	    	super();
	    }

	    @Override
	    public Class<?> getColumnClass(int columnIndex) {
	      @SuppressWarnings("rawtypes")
		Class clazz = String.class;
	      switch (columnIndex) {
	        case 0:
	          clazz = Boolean.class;
	          break;
	      }
	      return clazz;
	    }

	    @Override
	    public boolean isCellEditable(int row, int column) {
	    	boolean editable = false;
            if (column == 0) {
                Object value = getValueAt(row, column);
                if (value instanceof Boolean) {
                	if(value!=null){
                		editable = true;
                	}
                }
            }
            return editable;
	    }
	    
	    @Override
	    public Object getValueAt(int row, int column) {
	    	if(column==0){
	    		return (Boolean) super.getValueAt(row, column);
	    	}
	    	return super.getValueAt(row, column);
	    }

	  }
}

 
