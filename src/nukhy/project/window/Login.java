package nukhy.project.window;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import nukhy.project.Database.DatabaseHandler;

public class Login {

	public JFrame frame;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JLabel loadingLabel;
	private JButton btnLogin;
	private MainWindow mainWindow;
	/**
	 * Create the application.
	 * @param window 
	 */
	public Login(MainWindow window) {
		this.mainWindow = window;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 302, 228);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2-100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		ImageIcon icon = new ImageIcon("icon/icon.png");
		ImageIcon loader = new ImageIcon("icon/loading.gif");
		frame.setIconImage(icon.getImage());
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(56, 69, 63, 14);
		frame.getContentPane().add(lblUsername);
		
		txtUsername = new JTextField();
		lblUsername.setLabelFor(txtUsername);
		txtUsername.setBounds(56, 84, 176, 20);
		frame.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(56, 112, 46, 14);
		frame.getContentPane().add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(56, 125, 176, 20);
		frame.getContentPane().add(txtPassword);
		txtPassword.setColumns(10);
		
		btnLogin = new JButton("Login");
		lblPassword.setLabelFor(btnLogin);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadingLabel.setVisible(true);
				btnLogin.setEnabled(false);
				txtPassword.setEnabled(false);
				txtUsername.setEnabled(false);
				Thread db = new DatabaseLogin();
				db.start();
			}
		});
		btnLogin.setBounds(100, 156, 89, 23);
		frame.getContentPane().add(btnLogin);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setForeground(Color.LIGHT_GRAY);
		panel.setBounds(0, 0, 296, 60);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblSqlServerAuthentication = new JLabel("SQL SERVER AUTHENTICATION");
		lblSqlServerAuthentication.setHorizontalAlignment(SwingConstants.CENTER);
		lblSqlServerAuthentication.setBounds(0, 11, 296, 22);
		panel.add(lblSqlServerAuthentication);
		lblSqlServerAuthentication.setFont(new Font("Tahoma", Font.BOLD, 18));
		loadingLabel = new JLabel(loader);
		loadingLabel.setBounds(0, 46, 296, 16);
		loadingLabel.setVisible(false);
		frame.getContentPane().add(loadingLabel);
		frame.getRootPane().setDefaultButton(btnLogin);
	}
	
	class DatabaseLogin extends Thread{
		public void run(){
			try {
				sleep(1000);
			} catch (InterruptedException e) {

			}
			String username = txtUsername.getText();
			char[] password = txtPassword.getPassword();
			if(username.equals("") || password.equals("")){ 
				JOptionPane.showMessageDialog(frame, "<html><body><p style='width: 200px;'> Username or Password can't be left empty</p></body></html>", 
					    "ERROR",JOptionPane.ERROR_MESSAGE);
				loadingLabel.setVisible(false);
				btnLogin.setEnabled(true);
				txtPassword.setEnabled(true);
				txtUsername.setEnabled(true);
				return;
			}
			DatabaseHandler handler = new DatabaseHandler(username, new String(password));
			String hasil = handler.authDB();
			handler.closeCon();
			if(hasil.equals("Berhasil")){
				frame.setVisible(false);
				mainWindow.setLoginAuth(username, new String(password));
				mainWindow.frame.setVisible(true);
			}else{
				JOptionPane.showMessageDialog(frame, "<html><body><p style='width: 200px;'>"+hasil+"</p></body></html>", 
					    "ERROR",JOptionPane.ERROR_MESSAGE);
				loadingLabel.setVisible(false);
				btnLogin.setEnabled(true);
				txtPassword.setEnabled(true);
				txtUsername.setEnabled(true);
			}
			return;
		}
	}
}
