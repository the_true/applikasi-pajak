package nukhy.project.window;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.eclipse.wb.swing.FocusTraversalOnArray;

public class Options extends JDialog {

	private static final long serialVersionUID = 2425456058807506794L;
	private final JPanel contentPanel = new JPanel();
	private JButton btnCancel;
	private JButton btnSave;
	private JPanel buttonPane;
	private JLabel lblSelectDatabase;
	private JComboBox<String> comboBox;
	public String currentDb;

	/**
	 * Create the dialog.
	 */
	public void setCurrentDB(String currentdb){
		this.currentDb=currentdb;
	}
	public Options(ArrayList<String> dblist, String port, String currentDb) {
		setModal(true);
		setResizable(false);
		this.currentDb=currentDb;
		setBounds(100, 100, 326, 134);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 322, 58);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		{
			lblSelectDatabase = new JLabel("Select Database");
			lblSelectDatabase.setBounds(10, 11, 94, 14);
			contentPanel.add(lblSelectDatabase);
		}
		
		comboBox = new JComboBox<String>();
		for(String list:dblist){
			comboBox.addItem(list);
		}
		comboBox.setSelectedItem(currentDb);
		comboBox.setBounds(119, 8, 176, 20);
		contentPanel.add(comboBox);
		
		{
			buttonPane = new JPanel();
			buttonPane.setBounds(0, 57, 322, 48);
			getContentPane().add(buttonPane);
			buttonPane.setLayout(null);
			{
				btnSave = new JButton("OK");
				btnSave.setBounds(183, 11, 57, 23);
				buttonPane.add(btnSave);
				getRootPane().setDefaultButton(btnSave);
				btnSave.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setCurrentDB(comboBox.getSelectedItem().toString());
						dispose();
					}
				});
			}
			getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{contentPanel, buttonPane, btnSave, btnCancel}));
			{
				btnCancel = new JButton("Cancel");
				btnCancel.setBounds(247, 11, 65, 23);
				buttonPane.add(btnCancel);
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
			}
		}
	}
}
