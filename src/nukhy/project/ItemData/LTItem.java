package nukhy.project.ItemData;

public class LTItem {
	public String npwp,
	nama,
	jalan,
	blok,
	nomor,
	rt,
	rw,
	kecamatan,
	kelurahan,
	kabupaten,
	propinsi,
	kode,
	nomer_telpon;

	public LTItem(String npwp, String nama, String jalan, String blok,
			String nomor, String rt, String rw, String kecamatan,
			String kelurahan, String kabupaten, String propinsi, String kode,
			String nomer_telpon) {
		super();
		this.npwp = npwp;
		this.nama = nama;
		this.jalan = jalan;
		this.blok = blok;
		this.nomor = nomor;
		this.rt = rt;
		this.rw = rw;
		this.kecamatan = kecamatan;
		this.kelurahan = kelurahan;
		this.kabupaten = kabupaten;
		this.propinsi = propinsi;
		this.kode = kode;
		this.nomer_telpon = nomer_telpon;
	}
	
	public Object[] getArrayObject(){
		Object[] object = {null,"LT",npwp,nama,jalan,blok,nomor,rt,rw,kecamatan,kelurahan,
				kabupaten,propinsi,kode,nomer_telpon};
		return object;
	}
	
}
