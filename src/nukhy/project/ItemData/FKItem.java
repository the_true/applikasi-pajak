package nukhy.project.ItemData;

public class FKItem {
	public String kd_jenis_transaksi,
	fg_pengganti,
	nomor_faktur,
	masa_pajak,
	tahun_pajak,
	tanggal_faktur,
	npwp,
	nama,
	alamat_lengkap,
	jumlah_dpp,
	jumlah_ppn,
	jumlah_ppnbm,
	id_keterangan_tambahan,
	fg_uang_muka,
	uang_muka_dpp,
	uang_muka_ppn,
	uang_muka_ppnbm,
	referensi;

	public FKItem(String kd_jenis_transaksi, String fg_pengganti,
			String nomor_faktur, String masa_pajak, String tahun_pajak,
			String tanggal_faktur, String npwp, String nama,
			String alamat_lengkap, String jumlah_dpp, String jumlah_ppn,
			String jumlah_ppnbm, String id_keterangan_tambahan,
			String fg_uang_muka, String uang_muka_dpp, String uang_muka_ppn,
			String uang_muka_ppnbm, String referensi) {
		super();
		this.kd_jenis_transaksi = kd_jenis_transaksi;
		this.fg_pengganti = fg_pengganti;
		this.nomor_faktur = nomor_faktur;
		this.masa_pajak = masa_pajak;
		this.tahun_pajak = tahun_pajak;
		this.tanggal_faktur = tanggal_faktur;
		this.npwp = npwp;
		this.nama = nama;
		this.alamat_lengkap = alamat_lengkap;
		this.jumlah_dpp = jumlah_dpp;
		this.jumlah_ppn = jumlah_ppn;
		this.jumlah_ppnbm = jumlah_ppnbm;
		this.id_keterangan_tambahan = id_keterangan_tambahan;
		this.fg_uang_muka = fg_uang_muka;
		this.uang_muka_dpp = uang_muka_dpp;
		this.uang_muka_ppn = uang_muka_ppn;
		this.uang_muka_ppnbm = uang_muka_ppnbm;
		this.referensi = referensi;
	}
	
	public Object[] getArrayObject(){
		Object[] data = {true,"FK",kd_jenis_transaksi,fg_pengganti,nomor_faktur,
				masa_pajak,	tahun_pajak,tanggal_faktur,	npwp, nama, alamat_lengkap,	jumlah_dpp,
				jumlah_ppn,	jumlah_ppnbm,
				id_keterangan_tambahan,	fg_uang_muka, uang_muka_dpp,
				uang_muka_ppn, uang_muka_ppnbm, referensi};
		return data;
	}
	
}
