package nukhy.project.ItemData;

public class FMItem {
	private String kd_jenis_transaksi,fg_pengganti,nomor_faktur,masa_pajak,tahun_pajak,
	tanggal_faktur, npwp, nama, alamat_lengkap, jumlah_dpp, jumlah_ppn, jumlah_ppnbm,is_creditable;

	public FMItem(String kd_jenis_transaksi, String fg_pengganti,
			String nomor_faktur, String masa_pajak, String tahun_pajak,
			String tanggal_faktur, String npwp, String nama,
			String alamat_lengkap, String jumlah_dpp, String jumlah_ppn,
			String jumlah_ppnbm, String is_creditable) {
		this.kd_jenis_transaksi = kd_jenis_transaksi;
		this.fg_pengganti = fg_pengganti;
		this.nomor_faktur = nomor_faktur;
		this.masa_pajak = masa_pajak;
		this.tahun_pajak = tahun_pajak;
		this.tanggal_faktur = tanggal_faktur;
		this.npwp = npwp;
		this.nama = nama;
		this.alamat_lengkap = alamat_lengkap;
		this.jumlah_dpp = jumlah_dpp;
		this.jumlah_ppn = jumlah_ppn;
		this.jumlah_ppnbm = jumlah_ppnbm;
		this.is_creditable = is_creditable;
	}
	
	public Object[] getArrayObject(){
		Object[] data={true,"FM",kd_jenis_transaksi,fg_pengganti,nomor_faktur,masa_pajak,tahun_pajak,
				tanggal_faktur, npwp, nama, alamat_lengkap, jumlah_dpp, jumlah_ppn, jumlah_ppnbm,is_creditable};
		return data;
	}
}
