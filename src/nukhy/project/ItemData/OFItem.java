package nukhy.project.ItemData;

public class OFItem {
	public String kode_objek,
	nama,
	harga_satuan,
	jumlah_barang,
	harga_total,
	diskon,
	dpp,
	ppn,
	tarif_ppnbm,ppnbm;

	public OFItem(String kode_objek, String nama, String harga_satuan,
			String jumlah_barang, String harga_total, String diskon,
			String dpp, String ppn, String tarif_ppnbm,String ppnbm) {
		super();
		this.kode_objek = kode_objek;
		this.nama = nama;
		this.harga_satuan = harga_satuan;
		this.jumlah_barang = jumlah_barang;
		this.harga_total = harga_total;
		this.diskon = diskon;
		this.dpp = dpp;
		this.ppn = ppn;
		this.tarif_ppnbm = tarif_ppnbm;
		this.ppnbm=ppnbm;
	}
	
	public Object[] getArrayObject(){
		Object[] object = {null,"OF",kode_objek,	nama, harga_satuan, jumlah_barang, 
				harga_total,diskon,dpp,ppn,tarif_ppnbm,ppnbm};
		return object;
	}
	
}
