package nukhy.project.Data;

import nukhy.project.ItemData.FMItem;

public class DataPajakMasuk {
	public static Object[][] title={
		{null,"FM","KD_JENIS_TRANSAKSI","FG_PENGGANTI","NOMOR_FAKTUR","MASA_PAJAK","TAHUN_PAJAK","TANGGAL_PAJAK","NPWP","NAMA","ALAMAT_LENGKAP","JUMLAH_DPP",
			"JUMLAH_PPN","JUMLAH_PPNBM","IS_CREDITABLE"}
	};
	public FMItem of;
	public boolean exportData;
	
	public DataPajakMasuk(FMItem f){
		this.of =f;
		this.exportData=true;
	}
}
