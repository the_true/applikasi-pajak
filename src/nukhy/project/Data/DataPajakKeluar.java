package nukhy.project.Data;

import java.util.ArrayList;

import nukhy.project.ItemData.FKItem;
import nukhy.project.ItemData.LTItem;
import nukhy.project.ItemData.OFItem;


public class DataPajakKeluar {
	public final static Object[][] title=
		{{null,"FK","KD_JENIS_TRANSAKSI","FG_PENGGANTI","NOMOR_FAKTUR","MASA_PAJAK","TAHUN_PAJAK","TANGGAL_FAKTUR","NPWP","NAMA","ALAMAT_LENGKAP","JUMLAH_DPP",
			"JUMLAH_PPN","JUMLAH_PPNBM","ID_KETERANGAN_TAMBAHAN","FG_UANG_MUKA","UANG_MUKA_DPP","UANG_MUKA_PPN","UANG_MUKA_PPNBM","REFERENSI"},
		 {null,"LT","NPWP","NAMA","JALAN","BLOK","NOMOR","RT","RW","KECAMATAN","KELURAHAN","KABUPATEN","PROPINSI","KODE_POS","NOMER_TELEPON"
				,"","","","",""},
		 {null,"OF","KODE_OBJEK","NAMA","HARGA_SATUAN","JUMLAH_BARANG","HARGA_TOTAL","DISKON","DPP","PPN","TARIF_PPNBM","PPNBM","","","","","","","",""}
		};
	public Boolean exportData;
	public FKItem fk;
	public LTItem lt;
	public ArrayList<OFItem> of;
	public DataPajakKeluar(FKItem f,LTItem l,ArrayList<OFItem> o){
		this.fk =f;
		this.lt =l;
		this.of =o;
		this.exportData=true;
	}
	
	
}
