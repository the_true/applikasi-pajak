
public class Temp {

}
/*import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class Main {
	private JFrame frame;
	private JButton process;
	private JFileChooser saveTo;
	public static void main(String args[]){
	   try {
		      UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

		    } catch (Exception e) { System.err.println("Error: " + e.getMessage()); }
		new Main();
	}
	

	public Main(){
		CreateGUI();
		studentList = new ArrayList<Student>();
		process.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				//saveTo.showDialog(frame, "Save");
				int result = saveTo.showSaveDialog(frame);
			   if (result == JFileChooser.APPROVE_OPTION) {
			        try {
			        	File file = saveTo.getSelectedFile();
			        	if(getFileExtension(file).toLowerCase()!="xlsx"){
			        		file = new File(saveTo.getSelectedFile().getAbsolutePath()+".xlsx");
			        	}
			        	readDatabase();
			        	writeToExcel(file);
			            //FileWriter fw = new FileWriter(saveTo.getSelectedFile()+".txt");
			            //fw.write(sb.toString());
			        	System.out.println("save to excel");
			        } catch (Exception ex) {
			            ex.printStackTrace();
			        }
			    }
			}
		});
	}
	
	private String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf("."));

	    } catch (Exception e) {
	        return "";
	    }

	}
	
	public void CreateGUI(){
		frame = new JFrame("Demo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		process = new JButton("Export to XSL");
		process.setSize(50, 20);

		frame.getContentPane().add(process,BorderLayout.SOUTH);
		
		frame.setSize(300, 200);
		frame.setVisible(true);
		
		saveTo = new JFileChooser();
		
		
	}
	
	/*public void readDatabase(){
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:example.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM STUDENT;" );
	      while ( rs.next() ) {
	    	  Student student = new Student(rs.getInt("nik"),rs.getString("firstname"),
	    			  rs.getString("lastname"),rs.getInt("class"),rs.getInt("total_grade"));
	    	  studentList.add(student);
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");
	}*/

	/*public void writeToExcel(File filename){
		Workbook workbook = new XSSFWorkbook();
		Sheet studentSheet = workbook.createSheet("Students");
		int rowIndex=1;
		Row row = studentSheet.createRow(0);
		
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName(HSSFFont.FONT_ARIAL);
		font.setFontHeightInPoints((short)10);
		font.setBold(true);
		style.setFont(font);
		for(int j = 0; j<5; j++)
			row.getCell(j).setCellStyle(style);
		for(Student student:studentList){
			row = studentSheet.createRow(rowIndex++);
			int cellIndex=0;
			row.createCell(cellIndex++).setCellValue(student.getNik());
			row.createCell(cellIndex++).setCellValue(student.getFname());
			row.createCell(cellIndex++).setCellValue(student.getLname());
			row.createCell(cellIndex++).setCellValue(student.getKelas());
			row.createCell(cellIndex++).setCellValue(student.getGrade());
		}
		for(int j = 0; j<5; j++)
			studentSheet.autoSizeColumn(j);
		try{
			FileOutputStream fos = new FileOutputStream(filename);
			workbook.write(fos);
			fos.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
}
*/